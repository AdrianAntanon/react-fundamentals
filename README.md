## Notes

### State

Before React 16.8 we used this to manage state, and it's only available like this in components that extend Component and in class-based components:

```javascript
state = {
    persons: [
      { name: 'Adri', age: 27 },
      { name: 'Alex', age: 26 },
      { name: 'Andrea', age: 26 },
    ],
  };

    switchNameHandler = () => {
    this.setState({
      persons: [
        { name: 'Adrián', age: 27 },
        { name: 'Alex', age: 26 },
        { name: 'Andrea', age: 26 },
      ],
    });
  }

  render() {
    return (
      <div className="App">
        ...
        <button onClick={this.switchNameHandler}>Switch Name</button>
        <Person name={this.state.persons[0].name} age={this.state.persons[0].age}>
        ...
      </div>
    )
  };
```

After the release of React 16.8 and the implementation of React Hooks, it changed how this works a bit, to make it more comfortable.

We just need to import useState from React and it works like this

```javascript
  const [personsState, setPersonsState] = useState({
    persons: [
      { name: 'Adri', age: 27 },
      { name: 'Alex', age: 26 },
      { name: 'Andrea', age: 26 },
    ]
  });
  const switchNameHandler = () => {
    setPersonsState({
      persons: [
        { name: 'Adrián', age: 27 },
        { name: 'Alex', age: 26 },
        { name: 'Andrea', age: 26 },
      ],
    });
  }

  return (
    <div className="App">
        ...
        <button onClick={switchNameHandler}>Switch Name</button>
        <Person name={personsState.persons[0].name} age={personsState.persons[0].age}>
        ...
      </div>
  );
```

### Conditional lists

We have two ways to make a conditional list in React, one of them is to use a ternary operator like this

```javascript
    state = {
      persons: [
        { name: 'Adri', age: 27 },
      ],
      showPersons: false
    };

    togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  }

  return (
    <div className="App">
      ...
      <button onClick={this.togglePersonsHandler}>Toggle Persons</button>
      {this.state.showPersons ?
        <Person name={personsState.persons[0].name} age={personsState.persons[0].age}>
        :
        <p>No content</p>
      }
      ...
    </div>
  );
```

And the other way is to do it in "JavaScript Way", outside of the return we can create an object as in the following example:

```javascript
let persons = null;

if (this.state.showPersons) {
    persons = (
        <div>
            {this.state.persons.map((person, index) => {
                return (
                    <Person
                        key={person.id}
                        click={() => this.deletePersonHandler(index)}
                        name={person.name}
                        age={person.age}
                        changed={(event) => this.nameChangeHandler(event, person.id)}
                    />
                );
            })}
        </div>
    );
}

return (
    <div className="App">
        ...
        <button onClick={this.togglePersonsHandler}>Toggle Persons</button>
        {persons}
        ...
    </div>
);
```
