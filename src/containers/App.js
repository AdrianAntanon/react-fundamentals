import React, { Component } from 'react';
import classes from './App.css';

import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {
  constructor(props) {
    super(props);
    console.log('[App.js] constructor');
  }
  state = {
    persons: [
      { id: '1', name: 'Adri', age: 27 },
      { id: '2', name: 'Alex', age: 26 },
      { id: '3', name: 'Andrea', age: 26 },
    ],
    showPersons: false,
    authenticated: false
  };

  deletePersonHandler = (personIndex) => {
    const currentPersons = [...this.state.persons];
    currentPersons.splice(personIndex, 1);
    this.setState({ persons: currentPersons })
  };

  nameChangeHandler = (event, id) => {
    // Recogemos el índice, que nos lo devolverá una vez obtengamos un true
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    // Hacemos una copia de la información que hay en ese índice
    const person = {
      ...this.state.persons[personIndex]
    };

    // El parámetro dentro de nuestra copia es igual a la información que introduce el usuario mediante el input
    person.name = event.target.value;

    // Hacemos una copia ENTERA de nuestro array de información
    const persons = [...this.state.persons];
    // Y modificamos la parte exacta pasándole el índice y asignándole la información modificada de person
    persons[personIndex] = person;

    // Todo esto lo podemos hacer porque aunque sea una constante, realmente lo único que es fijo es la POSICIÓN EN MEMORIA donde se guarda, no la información en sí, por lo que podemos borrar y modificarla al gusto mientras no cambiemos el espacio en memoria que atacamos.


    // Asignamos un nuevo valor al array persons, para que obtenga toda la información que hemos guardado en el segundo persons
    this.setState({ persons: persons });
  };

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  }

  loginHandler = () => {
    this.setState({ authenticated: true })
  };

  render() {

    let persons = null;

    if (this.state.showPersons) {
      persons = (
        <div>
          <Persons
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.nameChangeHandler}
          />
        </div>
      );
    };

    return (
      <div className={classes.App}>
        <Cockpit
          title={this.props.appTitle}
          persons={this.state.persons}
          showPersons={this.state.showPersons}
          clicked={this.togglePersonsHandler}
          login={this.loginHandler}
        />
        {persons}
      </div>
    );
  }
}

export default App;
