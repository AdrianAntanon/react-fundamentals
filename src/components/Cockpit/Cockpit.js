import React, { useEffect, useRef } from 'react';

import classes from './Cockpit.css';

const Cockpit = (props) => {
  const { persons, showPersons, clicked, title, login } = props;
  const toggleBtnRef = useRef(null);

  useEffect(() => {
    console.log('[Cockpit.js] useEffect');
    // Http request...
    // setTimeout(() => {
    //   // alert('Saved data to cloud!');
    // }, 1000);
    toggleBtnRef.current.click();
    return () => {
      console.log('[Cockpit.js] cleaning work in useEffect');
    };
  }, []);

  useEffect(() => {
    console.log('[Cockpit.js] 2n useEffect');
    return () => {
      console.log('[Cockpit.js] cleaning work in 2n useEffect');
    };
  });

  const assignedClasses = [];
  let btnClass = '';

  if (showPersons) {
    btnClass = classes.Red;
  }

  if (persons.length <= 2) {
    assignedClasses.push(classes.red);
  }

  if (persons.length <= 1) {
    assignedClasses.push(classes.bold);
  }

  return (
    <div className={classes.Cockpit}>
      <h1>{title}</h1>
      <p className={assignedClasses.join(' ')} >This is really working!</p>
      <button
        className={btnClass}
        onClick={clicked}
        ref={toggleBtnRef}
      >
        Toggle Persons
        </button>
      <button onClick={login}>Log in</button>
    </div>
  );
}

export default Cockpit;
