import React from 'react';

import classes from './Person.css';

const Person = (props) => {
  const { name, age, children, click, changed } = props;
  return (
    <div className={classes.Person}>
      <p onClick={click} >I'm a {name} and I am {age} years old!</p>
      {children}
      <input type="text" onChange={changed} />
    </div>
  );
}

export default Person;